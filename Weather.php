<?php
error_reporting(E_ERROR);
include("weatherInterface.php");

/*
* This class posts url with the parameters and gets the
* response.
* The response is then formatted.
*/
Class Weather
{

	public $url;
	public $data;
	public $jsonData;

	function __construct($url, $data, $jsonData)
	{
		$this->url = $url;
		$this->data = $data;
		$this->jsonData = $jsonData;
	}
	
	function getCityWeather($url, $data)
	{
		$createParams = array('http' => 
			array('method' => 'POST',
			'header' => 'Content-type: application/json', 
			'charset' => 'utf-8', 
			'content' => $data
			));
	
		$sendContext = stream_context_create($createParams);
		$decodedResponse = json_decode(file_get_contents($url, false, $sendContext), true);

		return $this->arrayData($decodedResponse);
	}

	function convertCelsius($kelvin)
	{
		$tempCelsius = ($kelvin - 273.15);

		return $tempCelsius;
	}

	function arrayData($jsonData)
	{
		$arrayCompressed = [];	

		$city = $jsonData['city']['name'];
		$celsius = $jsonData['list']['0']['main']['temp'];
		$pressure = $jsonData['list']['0']['main']['pressure'];
		$humidity = $jsonData['list']['0']['main']['humidity'];
		
		array_push($arrayCompressed, $pressure, $humidity, $city, $celsius);

		return $arrayCompressed;
	}

}
?>