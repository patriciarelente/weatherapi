<?php
include_once("Weather.php");
include_once("weatherInterface.php");

/*
* This class gets and sets all the parameters needed
* in Weather class.
*/

Class Response implements weatherInterface
{
	public $city;

	function __construct($city)
	{
		$this->city = $city;
	}

	function getWeather($city)
	{
		$data = "id=$city".
			"&APPID=1a6695438a46bc8e5df53c1007a21827&units=metric";
		$url = "http://api.openweathermap.org/data/2.5/forecast?".
			$data;

		$weatherResult = new Weather();
		$getResult = $weatherResult->getCityWeather($url, $data);

		return $getResult;
	}
}

?>